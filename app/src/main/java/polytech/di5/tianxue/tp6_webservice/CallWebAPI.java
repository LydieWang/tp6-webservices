package polytech.di5.tianxue.tp6_webservice;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class CallWebAPI extends AsyncTask<String, Void, GeoIP> {

    @Override
    protected GeoIP doInBackground(String... params) {
        GeoIP geoIP = null;
        URL url;
        String addrIP;

        try {
            url = new URL(params[0]);
            addrIP = params[1];

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            // Set POST and post data
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("contentType", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Length", String.valueOf(addrIP.getBytes().length));
            urlConnection.setDoOutput(true);
            urlConnection.getOutputStream().write(addrIP.getBytes());
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            // XML parser
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            geoIP = parseXML(parser);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//            result = new StringBuilder();
//            String line;
//            while((line = reader.readLine()) != null) {
//                result.append(line);
//            }
//            in.close();

        return geoIP;
    }

    protected void onPostExecute(GeoIP geoIP) {
        mListener.readResult(geoIP);
    }

    // Listener pour textView
    public interface TextViewListener {
        void readResult(GeoIP result);
    }

    private TextViewListener mListener;

    public void setListener(TextViewListener listener) {
        mListener = listener;
    }

    private GeoIP parseXML(XmlPullParser parser) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        GeoIP geoIP = new GeoIP();
        int counter = 0;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;

            switch (eventType) {
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    String status = "";

                    switch (name){
                        case "status":
                            status = parser.nextText();
                            geoIP.setStatus(status);
                            break;
                        case "message":
                            // Set message only if it fails
                            if (status.equals("fail")) {
                                geoIP.setMsg(parser.nextText());
                            }
                            break;
                        case "country":
                            geoIP.setCountryName(parser.nextText());
                            break;
                        case "countryCode":
                            geoIP.setCountryCode(parser.nextText());
                            break;
                        case "regionName":
                            geoIP.setRegionName(parser.nextText());
                            break;
                        case "city":
                            geoIP.setCity(parser.nextText());
                            break;
                        case "zip":
                            geoIP.setZip(parser.nextText());
                            break;
                        case "query":
                            Log.v("TEST","Hello"+counter);
                            counter++;
                            if (counter == 2)
                                geoIP.setIP(parser.nextText());
                            break;
                    }
                case XmlPullParser.END_TAG:
                    break;
            } // end switch
            eventType = parser.next();
        } // end while
        return geoIP;
    }

} // end CallAPI


