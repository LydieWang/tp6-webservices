package polytech.di5.tianxue.tp6_webservice;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoIP implements Parcelable {
    private String status;
    private String IP;
    private String CountryName;
    private String CountryCode;
    private String regionName;
    private String city;
    private String zip;
    private String msg;

    public GeoIP(){

    }

    protected GeoIP(Parcel in) {
        status = in.readString();
        IP = in.readString();
        CountryName = in.readString();
        CountryCode = in.readString();
        regionName = in.readString();
        city = in.readString();
        zip = in.readString();
        msg = in.readString();
    }

    public static final Creator<GeoIP> CREATOR = new Creator<GeoIP>() {
        @Override
        public GeoIP createFromParcel(Parcel in) {
            return new GeoIP(in);
        }

        @Override
        public GeoIP[] newArray(int size) {
            return new GeoIP[size];
        }
    };

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "GeoIP{" +
                "status='" + status + '\'' +
                ", IP='" + IP + '\'' +
                ", CountryName='" + CountryName + '\'' +
                ", CountryCode='" + CountryCode + '\'' +
                ", regionName='" + regionName + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(IP);
        dest.writeString(CountryName);
        dest.writeString(CountryCode);
        dest.writeString(regionName);
        dest.writeString(city);
        dest.writeString(zip);
        dest.writeString(msg);
    }
}