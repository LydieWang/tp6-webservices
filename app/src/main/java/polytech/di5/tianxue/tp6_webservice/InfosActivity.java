package polytech.di5.tianxue.tp6_webservice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InfosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infos);
        TextView txtInfo = findViewById(R.id.txt_infos);
        GeoIP geoIP;
        StringBuilder stringBuilder;

        if (getIntent() != null && getIntent().getExtras() != null) {
            // Get data from old activity
            geoIP = getIntent().getExtras().getParcelable("ip");
            stringBuilder = new StringBuilder();

            if (geoIP != null) {
                if (geoIP.getStatus().equals("success")) {
                    stringBuilder.append("IP used for this query : ");
                    stringBuilder.append(geoIP.getIP());
                    stringBuilder.append("\n");

                    stringBuilder.append("Country code : ");
                    stringBuilder.append(geoIP.getCountryCode());
                    stringBuilder.append("\n");

                    stringBuilder.append("Country name : ");
                    stringBuilder.append(geoIP.getCountryName());
                    stringBuilder.append("\n");

                    stringBuilder.append("Region name : ");
                    stringBuilder.append(geoIP.getRegionName());
                    stringBuilder.append("\n");

                    stringBuilder.append("City : ");
                    stringBuilder.append(geoIP.getCity());
                    stringBuilder.append("\n");

                    stringBuilder.append("Postal code : ");
                    stringBuilder.append(geoIP.getZip());
                    stringBuilder.append("\n");
                } else {
                    stringBuilder.append("Error message: ");
                    stringBuilder.append(geoIP.getMsg());
                    stringBuilder.append("\n");
                }

                // Set textview
                txtInfo.setText(stringBuilder.toString());
            }
        }
    }
}
