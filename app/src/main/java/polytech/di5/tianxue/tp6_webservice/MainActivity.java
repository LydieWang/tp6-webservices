package polytech.di5.tianxue.tp6_webservice;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private Button buttonGo;
    private EditText editTextIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonGo = findViewById(R.id.btnGo);
        textView = findViewById(R.id.txt);
        editTextIP = findViewById(R.id.editTextIP);

        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String addrIP = editTextIP.getText().toString();

                URL url = null;
                try {
                    url = new URL("http://ip-api.com/xml");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                CallWebAPI c = new CallWebAPI();
                c.setListener(new CallWebAPI.TextViewListener() {
                    @Override
                    public void readResult(GeoIP result) {
                        Intent intent = new Intent(MainActivity.this,InfosActivity.class);
                        intent.putExtra("ip", result);
                        startActivity(intent);
                    }
                });

                if (url != null)
                    c.execute(url.toString(), addrIP);
            }
        });
    }

    private TextView textView;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        buttonGo = findViewById(R.id.btnGo);
//        textView = findViewById(R.id.txt);
//
//        buttonGo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                // First way
//                Thread networkThread = new NetworkThread();
//                networkThread.start();
//
//
//                // Second way
//                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//                StrictMode.setThreadPolicy(policy);
//
//                URL url;
//                HttpURLConnection urlConnection =null;
//                try {
//                    url = new URL("http://www.google.com/");
//                    urlConnection = (HttpURLConnection) url.openConnection();
//                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                    final String content = readStream(in);
//                    textView.setText(content);
//                    urlConnection.disconnect();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    urlConnection.disconnect();
//                }
//
//
//                // Third way
//                URL url = null;
//                try {
//                    url = new URL("http://www.google.com/");
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
//                CallWebAPI c = new CallWebAPI();
//                c.setListener(new CallWebAPI.TextViewListener() {
//                    @Override
//                    public void printContent(String content) {
//                        textView.setText(content);
//                    }
//                });
//                c.execute(url);
//
//            }
//        });
//    }
//
//    private class NetworkThread extends Thread{
//        URL url;
//        HttpURLConnection urlConnection =null;
//        @Override
//        public void run(){
//            try {
//                url = new URL("http://www.google.com/");
//                urlConnection = (HttpURLConnection) url.openConnection();
//                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                final String content = readStream(in);
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        textView.setText(content);
//                    }
//                });
//
//                urlConnection.disconnect();
//            } catch (Exception e) {
//                e.printStackTrace();
//                urlConnection.disconnect();
//            }
//        }
//    }
//
//    private String readStream(InputStream in) throws IOException {
//        BufferedReader r = new BufferedReader(new InputStreamReader(in));
//        StringBuilder total = new StringBuilder();
//        String line;
//        while ((line = r.readLine()) != null) {
//            total.append(line).append('\n');
//        }
//        return total.toString();
//    }
}
